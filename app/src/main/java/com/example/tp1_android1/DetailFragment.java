package com.example.tp1_android1;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tp1_android1.data.Country;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;



public class DetailFragment extends Fragment {

    TextView nomPays;
    ImageView drapeau;
    TextView capitale;
    TextView langues;
    TextView monnaie;
    TextView population;
    TextView superficie;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nomPays = view.findViewById(R.id.nom_pays);
        drapeau = view.findViewById(R.id.drapeau);
        capitale = view.findViewById(R.id.capitale);
        langues = view.findViewById(R.id.langues);
        monnaie = view.findViewById(R.id.monnaie);
        population = view.findViewById(R.id.population);
        superficie = view.findViewById(R.id.superficie);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        nomPays.setText(Country.countries[args.getCountryId()].getName());

        String uri = Country.countries[args.getCountryId()].getImgUri();
        Context c = drapeau.getContext();
        drapeau.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri, null , c.getPackageName())));
        capitale.setEnabled(false);
        capitale.setText(Country.countries[args.getCountryId()].getCapital());
        langues.setEnabled(false);
        langues.setText(Country.countries[args.getCountryId()].getLanguage());
        monnaie.setEnabled(false);
        monnaie.setText(Country.countries[args.getCountryId()].getCurrency());
        population.setEnabled(false);
        population.setText(Country.countries[args.getCountryId()].getPopulation()+ "");
        superficie.setEnabled(false);
        superficie.setText(Country.countries[args.getCountryId()].getArea() + "km²");

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}


